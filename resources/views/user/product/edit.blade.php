@extends('user.product.layouts.base')
@section('content')
    <h1>Editing "{{ $product->name }}"</h1>
    <p class="lead">Edit and save this product below, or <a href="{{ route('products.index') }}">go back to all products.</a>
    <a href="{{URL::previous()}}" class="btn btn-primary float-right">Back</a></p>
    <hr>

    <form method="post" action={{route('products.update', $product->id)}}>
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="name" class="control-label">{{ __('Name') }}</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}" required>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="description" class="control-label">{{ __('Description') }}</label>
                <textarea rows="4" cols="40" id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>
                    {{$product->description}}
                </textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
        </div>

        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="category_id" class="control-label">{{ __('Category') }}</label>
                <select name="category_id" id="category_id" class="form-control">
                    @foreach($categories as $category)
                        @if($category->id == $product->category_id)
                            <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                        @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endif
                    @endforeach
                </select>
                @error('category_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="price" class="control-label">{{ __('Price') }}</label>
                <input id="price" type="number" step="0.5" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $product->price }}" required>
                @error('price')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="expire_at" class="control-label">{{ __('Expire at') }}</label>
                <input id="expire_at" type="date"  name="expire_at" value="{{ $product->expire_at }}" required>
                @error('expire_at')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <hr>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update Product') }}
                </button>
            </div>
        </div>
    </form>
@endsection
