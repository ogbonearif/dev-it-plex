<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{ asset('jquery-3.4.1.min.js') }}" defer></script>
    <script src="{{ asset('bootstrap-4.0.0-alpha.6-dist/js/bootstrap.min.js') }}" defer></script>
    <link href="{{ asset('bootstrap-4.0.0-alpha.6-dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset('css/base.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="container">
        @yield("content")
    </div>
</body>
</html>
