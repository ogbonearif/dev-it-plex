@extends('user.product.layouts.base')
@section('content')
    <h1>{{ $product->name }}</h1>
    <p class="lead">{{ $product->description }}</p>
    <hr>
    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit Product</a>
    <a href="{{ route('products.index') }}" class="btn btn-info">Back to all products</a>

    <form action="{{ route('products.destroy', $product->id)}}" method="post"
          class="inline-block float-right" onsubmit="return confirm('Are you sure you want to delete this product?');">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <input type="submit" value="Delete this Product" class="btn btn-danger">
    </form>
@endsection
