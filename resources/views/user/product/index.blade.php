@extends('user.product.layouts.base')
@section('content')
    <div>
        <h1>List of Products</h1>

        @if(Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong><em>{{Session::get('success')}}</em></strong>
            </div>
        @endif
        @if(! $products->isEmpty())
        <p>Here is a list of all your products. <a href="{{ route('products.create') }}">Add a new one ?</a></p>
            <table border="1pt">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Category</th>
                <th>Price</th>
                <th>Expiration date</th>
                <th colspan="2">Actions on Product</th>
            </tr>
            </thead>
            @foreach($products as $product)
                <tbody>
                <tr>
                    {{--@php
                        $categories = App\Category::all();
                    @endphp--}}
                    <td>{{$product->name}}</td>
                    <td>{{$product->description}}</td>
                    {{--<td>{{$categories->where('id', $product->category_id)->first()->name}}</td>--}}
                    <td>{{$product->category->name}}</td>
                    <td>{{$product->price}} F</td>
                    <td>{{$product->expire_at}}</td>
                    <td><a href="{{ route('products.show', $product->id) }}" class="btn btn-info">View</a></td>
                    <td><a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit</a></td>
                </tr>
                </tbody>
            @endforeach
        </table>
        @else
            <h4><em>No product available</em></h4>
        @endif
    </div>
    <a href="{{route('user.home')}}">Home</a>
@endsection
