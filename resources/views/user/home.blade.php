@extends('user.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in user !
                </div>
            </div>
        </div>
    </div>
</div>

<div align="center">
    <a href="{{route('products.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Create a new product</a>
    <hr>
    <a href="{{route('products.index')}}" class="btn btn-primary"><i class="fa fa-list"></i> Display all products</a>
</div>
@endsection
