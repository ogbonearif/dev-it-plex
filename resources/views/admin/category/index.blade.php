@extends('admin.category.layouts.base')
@section('content')
<!--div align="center"-->
    <h1>List of Categories</h1>
    @if(Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong><em>{{Session::get('success')}}</em></strong>
        </div>
    @endif
    @if(! $categories->isEmpty())
    <p>Here is a list of all your categories. <a href="{{ route('categories.create') }}">Add a new one ?</a></p>
    <table border="1pt">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th colspan="2">Actions on Category</th>

            </tr>
        </thead>
        @foreach($categories as $category)
            <tbody>
                <tr>
                    <td>{{$category->name}}</td>
                    <td>{{$category->description}}</td>
                    <td><a href="{{ route('categories.show', $category->id) }}" class="btn btn-info">View</a></td>
                    <td><a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Edit</a></td>
                </tr>
            </tbody>
        @endforeach
    </table>
    @else
        <h4><em>No Category available</em></h4>
    @endif
    <a href="{{route('admin.home')}}">Home</a>
<!--/div-->
@endsection
