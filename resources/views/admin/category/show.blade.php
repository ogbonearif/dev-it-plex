@extends('admin.category.layouts.base')
@section('content')
    <h1>{{ $category->name }}</h1>
    <p class="lead">{{ $category->description }}</p>
    <hr>
    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Edit Category</a>
    <a href="{{ route('categories.index') }}" class="btn btn-info">Back to all categories</a>

    <form action="{{ route('categories.destroy', $category->id)}}" method="post"
          class="inline-block float-right" onsubmit="return confirm('Are you sure you want to delete this category?');">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <input type="submit" value="Delete this Category" class="btn btn-danger">
    </form>
@endsection
