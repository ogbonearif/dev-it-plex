@extends('admin.category.layouts.base')
@section('content')
    <h1>Editing "{{ $category->name }}"</h1>
    <p class="lead">Edit and save this category below, or <a href="{{ route('categories.index') }}">go back to all categories.</a>
        <a href="{{URL::previous()}}" class="btn btn-primary float-right">Back</a></p>
    <hr>

    <form method="post" action={{route('categories.update', $category->id)}}>
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group">
                <label for="name" class="control-label">{{ __('Name') }}</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}" required>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-xs-6 col-sm col-md-6">
            <div class="form-group row">
                <label for="name" class="control-label">{{ __('Description') }}</label>
                <textarea rows="6" cols="40" id="name" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>
                    {{$category->description}}
                </textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update Category') }}
                </button>
            </div>
        </div>
    </form>
@endsection
