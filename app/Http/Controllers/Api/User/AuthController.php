<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('user.home');
        $this->middleware('guest:user');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }
        else{
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);
            return response()->json(['success'=>'User created successfully'], 200);
        }
    }

    public function showRegistrationForm()
    {
        return view('user.auth.register');
    }
    protected function guard()
    {
        return Auth::guard('user');
    }

    // login

    //use AuthenticatesUsers;

    public function username()
    {
        $champ = filter_var(request('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$champ => request(('username'))]);

        return $champ;
    }

    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    public function login(Request $request){
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        $user = User::all()->where('username', $request->username)->first();
        if ($user){
            return Hash::check($request->password, $user->password) ?
                response()->json(['success'=>'You are logged in user!'], 200) : response()->json(['error'=>'Unauthorised'], 401);
        }
        else return response()->json(['error'=>'Unauthorised'], 401);
    }
}
