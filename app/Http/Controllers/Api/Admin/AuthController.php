<?php

namespace App\Http\Controllers\Api\Admin;

use App\Admin;
use App\Category;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('admin.home');
        $this->middleware('guest:admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'unique:admins'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return Admin::create([
            'username' => $data['username'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 401);
        }
        else{
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            event(new Registered($admin = $this->create($request->all())));
            $this->guard()->login($admin);
            return response()->json(['success'=>'Admin created successfully'], 200);
        }
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

//Définition des fonction pour le login

    //use AuthenticatesUsers;

    public function username()
    {
        $champ = filter_var(request('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$champ => request(('username'))]);

        return $champ;
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request){
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
        $admin = Admin::all()->where('username', $request->username)->first();
        if ($admin){
            return Hash::check($request->password, $admin->password) ?
                response()->json(['success'=>'You are logged in admin!'], 200) : response()->json(['error'=>'Unauthorised'], 401);
        }
        else return response()->json(['error'=>'Unauthorised'], 401);
    }
}
