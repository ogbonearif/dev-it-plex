<?php

namespace App\Http\Controllers\User;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.product.index', ['products'=>Product::with('category')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.product.create', ['categories'=>Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|unique:products',
            'description' => 'required|min:5',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|numeric',
            'expire_at' => 'required|date|after:'.date("Y-m-d"),
        ]);

        $input = $request->all();

        Product::create($input);
        return redirect()->route('products.index')->with('success', "Product '".request('name')."' created successfully !!!");;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.product.show', ['product' => Product::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.product.edit', ['product' => Product::findOrFail($id)], ['categories'=>Category::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|min:3|'.Rule::unique('products')->ignore($id),
            'description' => 'required|min:5',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|numeric',
            'expire_at' => 'required|date|after:'.date("Y-m-d"),
        ]);

        $input = $request->all();
        $product->fill($input)->save();
        return redirect()->route('products.index')->with('success', 'Product updated successfully !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index')->with('success', "Product '".$product->name."' deleted successfully !!!");
    }
}
