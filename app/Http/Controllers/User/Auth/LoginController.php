<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('user.home');
        $this->middleware('guest:user')->except('logout');
    }

    public function username()
    {
        $champ = filter_var(request('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$champ => request(('username'))]);

        return $champ;
    }

    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('user');
    }
}
