<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    //
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'category_id', 'price', 'expire_at'];

    public function category(){
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

}
