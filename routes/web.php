<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

//Auth::routes();

Route::prefix('user')->namespace('User')->name('user.')->group(function () {
    Auth::routes();

    Route::get('/home', 'UserHomeController@index')->name('home');
});

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function () {
    Auth::routes();

    Route::get('/home', 'AdminHomeController@index')->name('home');
});

//Route::resource('categories', 'CategoryController');

Route::prefix('user')->namespace('User')->group(function () {
    Route::resource('products', 'ProductController');
});

Route::prefix('admin')->namespace('Admin')->group(function () {
    Route::resource('categories', 'CategoryController');
});



















// Définitions des routes pour 'User'
/*Route::get('user/login', 'User\Auth\LoginController@showLoginForm')->name('user.login');
Route::post('user/login', 'User\Auth\LoginController@login');
Route::get('user/register', 'User\Auth\RegisterController@showRegistrationForm')->name('user.register');
Route::post('user/register', 'User\Auth\RegisterController@register');
Route::get('user/home', 'User\UserHomeController@index')->name('user.home');

Route::post('user/logout', 'User\Auth\LoginController@logout')->name('user.logout');

Route::post('user/password/email','User\Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
Route::post('user/password/reset', 'User\Auth\ResetPasswordController@reset')->name('user.password.update');
Route::get('user/password/reset', 'User\Auth\ForgotPasswordController@showLinkRequestForm')->name('user.password.request');
Route::get('user/password/reset/{token}', 'User\Auth\ResetPasswordController@showResetForm')->name('user.password.reset');


// Définitions des routes pour 'Admin'
Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\Auth\LoginController@login');
Route::get('admin/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('admin.register');
Route::post('admin/register', 'Admin\Auth\RegisterController@register');
Route::get('admin/home', 'Admin\AdminHomeController@index')->name('admin.home');

Route::post('admin/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

Route::post('admin/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::post('admin/password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::get('admin/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');*/

//Définitions des routes pour 'Category'

//Route::resource('categories', 'CategoryController');
/*Route::get('admin/categories/create', 'Admin\CategoryController@create')->name('categories.create');
Route::post('admin/categories', 'Admin\CategoryController@store')->name('categories.store');
Route::get('admin/categories', 'Admin\CategoryController@index')->name('categories.index');
Route::get('admin/categories/{category}', 'Admin\CategoryController@show')->name('categories.show');
Route::get('admin/categories/{category}/edit', 'Admin\CategoryController@edit')->name('categories.edit');
Route::put('admin/categories/{category}', 'Admin\CategoryController@update')->name('categories.update');
Route::delete('admin/categories/{category}', 'Admin\CategoryController@destroy')->name('categories.destroy');

//Définitions des routes pour 'Product'

Route::get('user/products/create', 'User\ProductController@create')->name('products.create');
Route::post('user/products', 'User\ProductController@store')->name('products.store');
Route::get('user/products', 'User\ProductController@index')->name('products.index');
Route::get('user/products/{product}', 'User\ProductController@show')->name('products.show');
Route::get('user/products/{product}/edit', 'User\ProductController@edit')->name('products.edit');
Route::put('user/products/{product}', 'User\ProductController@update')->name('products.update');
Route::delete('user/products/{product}', 'User\ProductController@destroy')->name('products.destroy');*/


