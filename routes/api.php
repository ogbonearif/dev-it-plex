<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test-get', function (Request $request){
    return response()->json(['server-ip' => $request->ip()]);
});

Route::post('test-post', function (Request $request){
    return response()->json(['username' => $request->username,
                             'password' => $request->password,
                            ]);
});

//
Route::prefix('admin')->namespace('Api\Admin')->name('admin.')->group( function (){
    Route::get('/login', 'AuthController@showLoginForm')->name('login');
    Route::post('/login', 'AuthController@login');
    Route::get('/register', 'AuthController@showRegistrationForm')->name('register');
    Route::post('/register', 'AuthController@register');
});

Route::prefix('admin')->namespace('Api\Admin')->group( function (){
    Route::get('/categories', 'CategoryController@index')->name('categories.index');
    Route::get('/categories/{id}/show', 'CategoryController@show')->name('categories.show');
    Route::post('/categories/create', 'CategoryController@create')->name('categories.create');
    Route::post('/categories', 'CategoryController@store')->name('categories.store');
    Route::post('/categories/{id}/update', 'CategoryController@update')->name('categories.update');
    Route::get('/categories/{id}/edit', 'CategoryController@edit')->name('categories.edit');
    Route::delete('/categories/{id}/delete', 'CategoryController@delete')->name('categories.destroy');
});

//
Route::prefix('user')->namespace('Api\User')->name('user.')->group( function (){
    Route::get('/login', 'AuthController@showLoginForm')->name('login');
    Route::post('/login', 'AuthController@login');
    Route::get('/register', 'AuthController@showRegistrationForm')->name('register');
    Route::post('/register', 'AuthController@register');
});

Route::prefix('admin')->namespace('Api\User')->group( function (){
    Route::get('/products', 'ProductController@index')->name('products.index');
    Route::get('/products/{id}/show', 'ProductController@show')->name('products.show');
    Route::post('/products/create', 'ProductController@create')->name('products.create');
    Route::post('/products', 'ProductController@store')->name('products.store');
    Route::post('/products/{id}/update', 'ProductController@update')->name('products.update');
    Route::get('/products/{id}/edit', 'ProductController@edit')->name('products.edit');
    Route::delete('/products/{id}/delete', 'ProductController@delete')->name('products.destroy');
} );





